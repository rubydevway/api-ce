object @user
attributes :id, :first_name,
        :last_name, :user_name,
        :email, :avatar,
        :cover_photo, :status,
        :followers_counter, :following_counter,
        :programs_counter,
        :sign_in_count

node('follower'){|entity| @current_user.follower?(entity.id)}