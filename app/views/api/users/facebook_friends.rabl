unless @entities.count.eql?(0)
  child @entities, :object_root => false, :root => "items" do
   node(false){|x| partial("api/users/_user", :object => x)}
   node('programs_images') { |x| partial('api/users/_program', :object => x.top_programs )}
  end
else
  node('items'){[]}
end

node(:pagination) do
  api_paginate @entities
end