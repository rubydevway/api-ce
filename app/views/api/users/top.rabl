object @users => false

attributes :id, :first_name,
        :last_name, :user_name,
        :email, :avatar,
        :cover_photo, :status,
        :followers_counter, :following_counter,
        :programs_counter

node('follower'){|entity| @current_user.follower?(entity.id)}

node('programs_images') { |entity| partial('api/users/_program', :object => entity.top_programs )}

