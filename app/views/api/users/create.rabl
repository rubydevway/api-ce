object false
node(:access_token) { |x|  @access_token.token }
node(:token_type) { |x|  "bearer"}

node(:user) { |x|  partial( "api/users/me", :object => @user) }
