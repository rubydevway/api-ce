object @entity => false
attributes :id, :text, :program_id, :created_at, :comments_counter
node(:owner) { |x| partial("api/program_comments/_author", :object => x.user) }
node('author'){|entity| entity.author?(@current_user)}
node('program_owner'){|entity| entity.program.owner?(@current_user)}
