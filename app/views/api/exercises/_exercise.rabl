attributes :id, :description, :likes_counter, :comments_counter

node(:stream_path) { |e| e.video(:original) }
node(:thumb_big_url) { |e| e.video(:big)}

node('liked'){ |e| e.liked?(@current_user) }

child(:program){ attributes :id, :name, :description }
child(:metrics){ attributes :metric_type, :value_1, :value_2, :value_3 }
child(:place){attributes :id, :address, :lat, :lng, :foursquare_id, :exercises_counter, :name}

child(:owner){ attributes :id, :user_name, :email }
