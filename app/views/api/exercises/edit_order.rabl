object @exercises => nil
attributes :id, :position, :description, :likes_counter, :comments_counter

node(:stream_path) { |e| e.video(:original) }
node(:thumb_big_url) { |e| e.video(:big)}

child(:program) do
 attributes :id, :name, :description, :exercises_counter, :favorites_counter, :comments_counter, :image
end

child(:owner) do
 attributes :id, :email, :user_name, :avatar
end

child(:exercise_type) do
  attributes :id, :name, :image, :exercises_counter
end

node(:metrics) { |e| partial( "api/feeds/metrics", :object => e.metrics) }