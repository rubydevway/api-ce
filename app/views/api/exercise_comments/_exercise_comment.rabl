object @entity => false
attributes :id, :text, :exercise_id, :created_at, :comments_counter
node(:owner) { |x| partial("api/exercise_comments/_author", :object => x.user) }
node('author'){|entity| entity.author?(@current_user)}
node('program_owner'){|entity| entity.exercise.program.owner?(@current_user)}
