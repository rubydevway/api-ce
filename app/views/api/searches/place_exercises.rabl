unless @entities.count.eql?(0)
  child @entities, :object_root => false, :root => "items" do |x|
     node(false) { |e| partial( "api/feeds/_exercise", :object => e)}
  end
else
  node('items'){[{}]}
end
node(:pagination) do
  api_paginate @entities
end
