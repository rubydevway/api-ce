unless @entities.count.eql?(0)
  child @entities, :object_root => false, :root => "items" do
    node(false) { |entity| partial('api/following/_follower', :object => entity.user )}
  end
else
  node('items'){[{}]}
end

node(:pagination) do
  api_paginate @entities
end