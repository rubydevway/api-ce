attributes :id, :notification_type, :entity_id, :created_at

node(false) { |x| partial('api/user_notifications/_program', :object => Program.find(x.entity_id)) if x.notification_type.eql?("favorite_program") || x.notification_type.eql?("program_comment") || x.notification_type.eql?("create_program")}
node(false) { |x| partial('api/user_notifications/_exercise', :object => Exercise.find(x.entity_id)) if x.notification_type.eql?("exercise_comment") || x.notification_type.eql?("like_exercise") || x.notification_type.eql?("create_exercise")}
node('owner') { |x| partial('api/user_notifications/_owner', :object => x.owner) }
node('member') { |x| partial('api/user_notifications/_owner', :object => x.user) }

