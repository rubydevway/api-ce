object @user
attributes :id, :user_name, :email, :avatar
node('follower'){|entity| @current_user.follower?(entity.id)}