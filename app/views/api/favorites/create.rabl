object nil
node(false) { |x|  partial( "api/favorites/_program", :object => @entity.program) }
node('favorite'){ |x| @entity.program.favorite?(@entity.user.id) }
node(:owner) { |x| partial( "api/users/user", :object => @entity.program.owner) }

