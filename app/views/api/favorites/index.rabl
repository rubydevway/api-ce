unless @entities.count.eql?(0)
  child @entities, :object_root => false, :root => "items" do
    node(false) { |x| partial( "api/favorites/_program", :object => x)}
    node('favorite'){ |x| x.favorite?(@current_user.id)}
    node(:owner) { |x| partial( "api/users/user", :object => x.owner )}
  end
else
  node('items'){[{}]}
end

node(:pagination) do
  api_paginate @entities
end