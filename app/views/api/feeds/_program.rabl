object @entities => nil
attributes :id, :name, :exercises_counter, :comments_counter, :image, :description, :favorites_counter
node('favorite'){ |x| x.favorite?(@current_user.id)}
node(:owner) { |e| partial( "api/users/user", :object => e.owner) }
