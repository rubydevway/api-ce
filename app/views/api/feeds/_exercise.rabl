object @entities => nil
attributes :id, :name, :description, :position, :likes_counter, :comments_counter, :created_at

node(:thumb_big_url) { |a| a.video(:big)}

node('liked'){ |e| e.liked?(@current_user) }

node(:thumb_medium_url) { |a| a.video(:medium)}
node(:stream_path) { |a| a.video.url(:original)}
node(:thumb_thumb_url) { |a| a.video(:thumb)}

child(:place) do
    attributes :id, :name, :address, :exercises_counter, :lat, :lng, :foursquare_id
end

child(:exercise_type) do
    attributes :id, :name, :exercises_counter, :image
    node('type_of_metric'){|x| x.metric.metric_type}
end

node(:metrics) { |e| partial( "api/feeds/metrics", :object => e.metrics) }
node(:program) { |e| partial( "api/feeds/program_short", :object => e.program) }
node(:owner) { |e| partial( "api/users/user", :object => e.owner) }
