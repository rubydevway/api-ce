object @entities => nil
attributes :id, :name, :exercises_counter, :image, :comments_counter
node('favorite'){ |x| x.favorite?(@current_user.id)}
