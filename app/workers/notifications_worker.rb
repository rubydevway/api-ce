require 'houston'

class NotificationsWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :notification_sender

  def perform(type, user_id, entity_id, message, owner_id)

   devices = Device.where(user_id: user_id)

   devices.first.user.increment!(:push_counter, 1) unless devices.first.user.nil?

   devices.each do |device|
     owner = User.find(owner_id)
     apn_clean_up
     notification = Houston::Notification.new(device: device.token)
     notification.alert = "#{owner.user_name} #{message}"
     notification.badge = device.user.push_counter
     notification.category = type
     notification.sound = "default"
     notification.custom_data = { resource_id: entity_id}
     apn.push(notification)
     puts "Send push for token: #{device.token}"

   end
  end

  def apn
    return @apn if @apn.present?
    @apn = Rails.env.production? ? Houston::Client.production : Houston::Client.development
    @apn.certificate = File.read(Rails.root.join("config", "apns", "stern_fit_staging.pem"))
    @apn
  end

  def apn_clean_up
    if apn.devices.any?
      puts "APN clean up"
      apn.devices.each do |token|
        puts "destroy: #{token}"
        Device.find_by_token(token.delete(' ')).destroy!
      end
    end
  end
end