class Admin::MetricsController < Admin::CrudController

  def index
    @q = Metric.ransack(params[:q])
    @entities = @q.result.page(params[:page]).per(params[:per_page])
  end

  private

  def set_class_name
    @class_name = 'Metric'
  end

  def entity_params
    params.require(@class_name.downcase.to_sym).permit(:param_1, :param_2, :param_3, :metric_type)
  end
end