class Admin::ReportsController < Admin::CrudController

  def index
    @q = Report.search(params[:q])
    @entities = @q.result.page(params[:page]).per(params[:per_page])
  end

  private
  def set_class_name
    @class_name = 'Report'
  end

  def entity_params
    params.require(:report).permit(:title, :description, :user_id)
  end

end
