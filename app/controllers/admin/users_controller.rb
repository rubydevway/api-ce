Devise::RegistrationsController

class Admin::UsersController < Admin::CrudController

  def authenticate_admin
    redirect_to new_user_session_path unless user_signed_in?
    redirect_to root_path if user_signed_in? && !(current_user.admin?)
  end

  def index
    @q = User.distinct.search(params[:q])
    @entities = @q.result.page(params[:page]).per(params[:per_page])
  end

  def update
    if @entity.update_without_password(entity_params)
      redirect_to admin_user_path(@entity), notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  private

  def set_class_name
    @class_name = 'User'
  end

  def entity_params
    params.require(:user).permit(:email, :first_name, :last_name, :role, :user_name, :password, :password_confirmation, :current_password)
  end
end