class Admin::ExerciseTypesController < Admin::CrudController

  def index
    @q = ExerciseType.ransack(params[:q])
    @entities = @q.result.page(params[:page]).per(params[:per_page])
  end

  private

  def set_class_name
    @class_name = 'ExerciseType'
  end

  def entity_params
    params.require(:exercise_type).permit(:name, :metric_id, :image)
  end
end