class Admin::AdminController < ActionController::Base

  protect_from_forgery with: :exception

  before_filter :auth_admin

  layout "admin"

  private

  def auth_admin
    warden.authenticate! scope: :user
    unless current_user.admin?
      flash[:notice] = "You don't have enough privileges!"
      redirect_to root_path
    end
  end
end