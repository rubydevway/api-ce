class Admin::ExercisesController < Admin::CrudController

  def index
    @q = Exercise.ransack(params[:q])
    @entities = @q.result.page(params[:page]).per(params[:per_page])
  end

  private

  def set_class_name
    @class_name = 'Exercise'
  end

  def entity_params
    params.require(@class_name.downcase.to_sym).permit(:description, :exercise_type_id, :video, :program_id)
  end
end