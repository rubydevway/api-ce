class Admin::ProgramsController < Admin::CrudController

  def index
    @q = Program.ransack(params[:q])
    @entities = @q.result.page(params[:page]).per(params[:per_page])
  end

  private

  def set_class_name
    @class_name = 'Program'
  end

  def entity_params
    params.require(@class_name.downcase.to_sym).permit(:name, :description, :image)
  end
end