class Api::ReportsController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def create
   @entity = Report.create(report_params)
   return head :ok, status: 200
  end

  private
  def report_params
    params.require(:report).permit(:title, :description).tap do |param|
      param[:user] = current_resource_owner
    end
  end
end