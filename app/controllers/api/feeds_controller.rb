class Api::FeedsController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def follow_ids
    Follower.where(:owner_id => @current_user.id).pluck(:user_id).count.eql?(0) ? @current_user.id : Follower.where(:owner_id => @current_user.id).pluck(:user_id) <<  @current_user.id
  end

  def programs
    @q = Program.search(user_id_in: follow_ids)
    @entities = @q.result.order(id: :desc).page(params[:page]).per(params[:per_page])
  end

  def program
    @entity = Program.find(params[:id])
    render '/api/programs/_program'
  end

  def program_exercises
    @q = Exercise.search(program_id_eq: params[:id])
    @entities = @q.result.order(position: :asc).page(params[:page]).per(params[:per_page])
    render '/api/feeds/exercises'
  end

  def exercises
    @q = Exercise.search(user_id_in: follow_ids)
    @entities = @q.result.order(id: :desc).page(params[:page]).per(params[:per_page])
  end
end