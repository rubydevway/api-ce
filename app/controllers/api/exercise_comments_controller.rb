class Api::ExerciseCommentsController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def index
    @q = ExerciseComment.search(exercise_id_eq: params[:id])
    @entities = @q.result.order(id: :desc).page(params[:page]).per(params[:per_page])
    render 'api/exercise_comments/index'
  end

  def create
    @entity = ExerciseComment.new(comment_params)
    if @entity.save
      UserNotification.create(notification_params) #unless Exercise.find(params[:id]).owner == current_resource_owner
      render 'api/exercise_comments/_exercise_comment'
    else
      respond_with @entity
    end
  end

  def destroy
    @entity = ExerciseComment.find(params[:id])
    if  @entity.owner?(@current_user)
      @entity.remove_notification
      @entity.destroy
      render 'api/exercise_comments/_exercise_comment'
    else
      respond_with @entity
    end
  end

  private
  def comment_params
    params.require(:comment).permit(:text).tap do |param|
      param[:user_id] = @current_user.id
      param[:exercise_id] = params[:id]
    end
  end

  def notification_params
    params.permit().tap do |param|
      param[:entity_id] = @entity.exercise.id
      param[:notification_type] = "exercise_comment"
      param[:owner] = current_resource_owner
      param[:user] = Exercise.find(params[:id]).owner
    end
  end
end