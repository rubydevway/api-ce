class Api::UsersController < Api::ApiController

  before_action only: [:me, :me_update] do
    doorkeeper_authorize!
  end

  def signin

    if params[:facebook_token].present?
      @user = User.get_by_fbtoken(params[:facebook_token])
    else
      @user = User.find_for_database_authentication(:email => params[:email])
      unless @user.present? && @user.valid_password?(params[:password])
        return render :json => { errors: { email: [ 'Invalid email/password combination.' ]}}, :status => :unauthorized
      end
    end

    sign_in(:user, @user)
    sign_out(@user)

    @access_token = Doorkeeper::AccessToken.create(:application_id => application.id, :resource_owner_id => @user.id)

    @current_user = @user

    return render 'api/users/create'
  end

  def me
    @user = current_resource_owner
  end

  def me_update

    @user = current_resource_owner

    if params[:user][:password].present? && !current_resource_owner.valid_password?(params[:user][:current_password])
      @user.errors.add(:current_password, 'is wrong')
      return respond_with @user
    else
      @user.avatar.clear if params[:delete_avatar]
      @user.cover_photo.clear if params[:delete_cover]

      @user.update(user_params)
    end

    return render 'api/users/show' if @user.save

    respond_with @user
  end

  def facebook_friends
    ids = []
    friends = @current_user.find_fb_friends
    friends.each do |friend|
      ids << friend['id']
    end

    @q = User.search(facebook_id_in: ids)
    @entities = @q.result.page(params[:page]).per(params[:per_page])
  end

  def facebook_activity
    follow_list = []

    if @current_user.facebook_id?
      facebook_friends.each{ |f| follow_list.push(@current_user.follow?(f.id)) }
    end

    render :json => {
        facebook_friends: @current_user.facebook_id? ? facebook_friends.count : 0,
        active: @current_user.facebook_id?,
        follow_all: follow_list.include?(false) ? false : true
    }, :status => 200
  end

  def show
    @user = User.find(params[:id])
    return render 'api/following/_follower'
  end

  def sign_up
    @user = User.create!(user_params)
    if @user.save
      sign_in(:user, @user)
      sign_out(@user)
      @access_token = Doorkeeper::AccessToken.create!(:application_id => application.id, :resource_owner_id => @user.id)
      return render 'api/users/create'
    else
      render :json => { errors: { message: [ "Email is invalid" ] } }, status: 500
    end
    respond_with @user
  end

  def reset_password
    params.require(:email)
    @user = User.where("lower(email) = ?", params[:email].downcase).first

    if @user.nil?
      return render :json => { errors: { exception: [ 'User with this email has not found' ]}}, :status => 404
    else
      @user.send_reset_password_instructions()
    end
    
    return head :no_content
  end

  def top
    @users = User.order('followers_counter DESC').limit(25).includes(:programs)
  end

  private

  def application
    raise 'Required client_id and client_secret' unless params['client_id'].present? && params['client_secret'].present?
    application = Doorkeeper::Application.find_by({:uid => params['client_id'], :secret => params['client_secret']})
    raise 'Application not found' if application.nil?
    application
  end

  def user_params
    params.require(:user).permit(:email, :password, :first_name, :last_name, :user_name, :avatar, :cover_photo, :status)
  end
end
