class Api::UserNotificationsController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def index
    ids = current_resource_owner.following.pluck(:user_id)
    @entities = []
    objects = {
        'start_following' => User,
        'program_comment' => Program,
        'exercise_comment' => Exercise,
        'like_exercise' => Exercise,
        'favorite_program' => Program,
    }
    @notifications = UserNotification.where('owner_id IN (?)', ids)
    @notifications.each do |notification|
      entity = objects[notification.notification_type].find(notification.entity_id)
      entity_owner = entity.class.name.eql?("User") ? entity : entity.owner
      unless entity_owner == current_resource_owner
        @entities.push(notification)
      end
    end
    @entities.first(50)
    @q = UserNotification.search(owner_id_in: ids)
    @entities = @q.result.order(id: :desc).limit(50)
  end

  def my_notifications
    @q = UserNotification.search({:m => 'and', user_id_eq: current_resource_owner.id, owner_id_not_eq: current_resource_owner.id })
    @entities = @q.result.order(id: :desc).limit(50)
  end
end
