class Api::ExercisesController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def create
    if params.has_key?(:place)
      @place = Place.find_by_foursquare_id params[:place][:foursquare_id]
      @place ||= Place.create(place_params)
    end

    @metric = ExerciseMetric.create(metric_params)
    @entity = Exercise.new(exercise_params)

    if @entity.save
      params[:user] =  @entity.owner
      @entity.increment_exercise_type_counter
      @entity.increment_program_counter
      UserNotification.create(notification_params)
      render :json => { message: [ 'exercise was created' ]}, :status => 200
    else
      render :json => { errors: { message: [ 'missing param' ]}}, :status => 500
    end
  end

  def destroy
    @entity = Exercise.find(params[:id])
    @entity.shift_position
    if @entity.owner?(@current_user)

      @entity.decrement_exercise_type_counter
      @entity.decrement_program_counter
      @entity.place.decrement_exercise_counter unless @entity.place.nil?
      @entity.remove_notification

      @entity.destroy
      #render 'api/feeds/_exercise'
      return head :ok, status: 200
    else
      return head :no_content, status: 204
    end
  end

  def user_exercises
    @q = Exercise.search(user_id_eq: params[:id])
    @entities = @q.result.order(id: :desc).page(params[:page]).per(params[:per_page])
    render 'api/feeds/exercises'
  end

  def my_exercises
    @q = Exercise.search(user_id_eq: @current_user.id)
    @entities = @q.result.order(id: :desc).page(params[:page]).per(params[:per_page])
    render 'api/feeds/exercises'
  end

  def show
    @entity = Exercise.find(params[:id])
  end

  def update
    @entity = Exercise.find(params[:id])
    if @entity.update!(exercise_params) && @entity.metrics.update!(metric_params)
      render '/api/exercises/show'
    end
  end

  def update_order
    params.permit!
    @entities = Exercise.update(params[:ids],params[:order])
    if @entities
      return head :ok, status: 200
    end
  end

  def edit_order
    @exercises = Exercise.where(program_id: params[:id]).order(position: :asc)
  end

  def likers
    likers = Like.where(exercise_id: params[:id]).pluck(:user_id)
    ids = likers.any? ? likers : 0

    @q = User.search(id_in: ids)
    @entities = @q.result.page(params[:page]).per(params[:per_page])
    render '/api/users/index'
  end

  def position
    Exercise.where(program_id: params[:exercise][:program_id]).count + 1
  end

  private
  def exercise_params
    params.require(:exercise).permit(:description, :video, :exercise_type_id, :program_id).tap do |param|
      param[:user_id] = @current_user.id
      param[:place_id] =  @place.id if @place
      param[:exercise_metric_id] = @metric.id if @metric
      param[:position] = position
    end
  end

  def metric_params
    params.require(:metrics).permit(:value_1, :value_2, :value_3, :metric_type)
  end

  def place_params
    params.require(:place).permit(:name, :lat, :lng, :address, :foursquare_id)
  end

  def notification_params
    params.permit().tap do |param|
      param[:entity_id] = @entity.id
      param[:notification_type] = "create_exercise"
      param[:owner] = current_resource_owner
      #param[:user] = Exercise.find(params[:exercise][:program_id]).owner
    end
  end
end