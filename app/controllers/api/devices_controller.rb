class Api::DevicesController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def add_device
    @device = Device.find_by(:token => params[:device][:token])

    if @device.present?
      @device.user = current_resource_owner
      return head :ok  if @device.save
    end

      @device = Device.create(device_params)
      return head :created if @device.save
      respond_with @device
  end

  def remove_device
    device = Device.find_by!(:token => params[:token])

    unless device.user == current_resource_owner
      return render :nothing => true,  :status => :forbidden
    end

    if device.destroy
      return render :nothing => true,  :status => :no_content
    end
  end

  def reset_counter
    current_resource_owner.reset_push_counter
    return head :no_content
  end

  private
  def device_params
    params.require(:device).permit(:token).tap do |param|
      param[:user] = current_resource_owner
    end
  end
end