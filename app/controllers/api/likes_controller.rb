class Api::LikesController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def create
    @entity = Like.new(like_params)
    if  @entity.save
      @notification = UserNotification.create(notification_params) #unless Exercise.find(params[:id]).owner == current_resource_owner

      #NotificationsWorker.perform_async(@notification.id)

      @entity = @entity.exercise
      render 'api/exercises/show'
    else
      respond_with @entity
    end
  end

  def destroy
    @like = Like.find_by!(like_params)
    @entity = @like.exercise

    @like.remove_notification
    @like.destroy
    render 'api/exercises/show'
  end

  private

  def like_params
    params.permit().tap do |whitelisted|
      whitelisted[:user_id] = current_resource_owner.id
      whitelisted[:exercise_id] = params[:id]
    end
  end

  def notification_params
    params.permit().tap do |param|
      param[:entity_id] = params[:id]
      param[:notification_type] = "like_exercise"
      param[:owner] = current_resource_owner
      param[:user] = Exercise.find(params[:id]).owner
    end
  end
end
