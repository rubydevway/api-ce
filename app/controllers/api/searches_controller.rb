class Api::SearchesController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def exercise_types
    @q = ExerciseType.search(params[:q])
    @q.sorts = ['exercises_counter desc','name asc']
    @entities = @q.result.page(params[:page]).per(params[:per_page]).limit(limit)
  end

  def programs
    @q = Program.search(params[:q])
    @q.sorts = ['favorites_counter desc','name asc']
    @entities = @q.result.page(params[:page]).per(params[:per_page]).limit(limit)
  end

  def athletes
    @q = User.search(params[:q])
    @q.sorts = ['followers_counter desc', 'user_name asc']
    @entities = @q.result.page(params[:page]).per(params[:per_page]).limit(limit)
  end

  def places
    @q = Place.search(params[:q])
    @q.sorts = ['exercises_counter desc', 'name asc']
    @entities = @q.result.page(params[:page]).per(params[:per_page]).limit(limit)
  end

  def limit
    params.has_key?(:q) ? 25 : 100
  end

  def type_exercises
    if params[:sort].eql?('heaviest')
      @q = Exercise.joins(:metrics).search(exercise_type_id_eq: params[:id])
      @entities = @q.result.except(:order).order('value_1 desc, description asc')
    else
      @q = Exercise.search(exercise_type_id_eq: params[:id])
      @q.sorts = sort
      @entities = @q.result
    end
     @entities = @entities.page(params[:page]).per(params[:per_page])
     render 'api/feeds/exercises'
  end

  def place_exercises
      @q = Exercise.search(place_id_eq: params[:id])
      @q.sorts = sort
      @entities = @q.result.page(params[:page]).per(params[:per_page])
      render 'api/feeds/exercises'
  end

  def sort
    sort_orders = {
        'liked' => 'likes_counter desc',
        'recent' => 'created_at desc',
        'description' => 'description asc'
    }
    [sort_orders[params[:sort]],sort_orders['description']]
  end
end