class Api::ProgramsController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def index
    @q = Program.search(query)
    @entities = @q.result.order(id: :desc).page(params[:page]).per(params[:per_page])
  end

  def create
    @entity = Program.new(program_params)
    if @entity.save
      @entity.increment_programs_counter
      UserNotification.create(notification_params)
      render 'api/programs/show'
    else
      render :json => { errors: { message: [ 'missing param: image || name || description' ]}}, :status => 500
    end
  end

  def destroy
    @entity = Program.find(params[:id])
    if @entity.owner?(@current_user)

      @entity.remove_notification
      @entity.decrement_programs_counter

      @entity.exercises.each do |exercise|
        exercise.place.decrement_exercise_counter unless exercise.place.nil?
        exercise.remove_notification unless exercise.nil?
      end

      @entity.destroy
      render 'api/programs/show'
    else
      return head :no_content, status: 204
    end
  end

  def update
    @entity = Program.find(params[:id])
    if @entity.owner?(@current_user) && @entity.update!(program_params)
      render '/api/programs/show'
    else
      respond_with @entity
    end
  end

  def user_programs
    @q = Program.search(user_id_eq: params[:id])
    @entities = @q.result.order(id: :desc).page(params[:page]).per(params[:per_page])
    render 'api/feeds/programs'
  end

  def my_programs
    @q = Program.search(user_id_eq: @current_user.id)
    @entities = @q.result.order(id: :desc).page(params[:page]).per(params[:per_page])
    render 'api/feeds/programs'
  end

  def query
    unless params[:q].present?
      {user_id_eq: @current_user.id}
    else
      params[:q].merge(user_id_eq: @current_user.id, m: 'and')
    end
  end

  private
  def program_params
    params.require(:program).permit(:name, :description, :image).tap do |param|
      param[:user_id] = @current_user.id
    end
  end

  def notification_params
    params.permit().tap do |param|
      param[:entity_id] = @entity.id
      param[:notification_type] = "create_program"
      param[:owner] = @current_user
    end
  end
end