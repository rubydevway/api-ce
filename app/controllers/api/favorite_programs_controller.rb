class Api::FavoriteProgramsController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def create
    @entity = FavoriteProgram.new(favourite_program_params)
    if @entity.save
      UserNotification.create(notification_params) #unless @current_user == Program.find(params[:id]).owner
      return render 'api/favorites/create'
    else
      respond_with @entity
    end
  end

  def destroy
    @entity = FavoriteProgram.where(favourite_program_params).first

    @entity.remove_notification unless @entity.nil?
    if @entity.destroy
      return render 'api/favorites/create'
    else
      respond_with @entity
    end
  end

  def user_favorites
    ids = FavoriteProgram.where(user_id: current_resource_owner.id).pluck(:program_id).count.eql?(0) ? 0 : FavoriteProgram.where(user_id: current_resource_owner.id).pluck(:program_id)

    @q = Program.search(id_in: ids)
    @q.sorts = ['name desc', 'favorites_counter desc']
    @entities = @q.result.page(params[:page]).per(params[:per_page])
    render 'api/favorites/index'
  end

  private
  def favourite_program_params
    params.permit().tap do |whitelisted|
      whitelisted[:user_id] = current_resource_owner.id
      whitelisted[:program_id] = params[:id]
    end
  end

  def notification_params
    params.permit().tap do |param|
      param[:entity_id] = params[:id]
      param[:notification_type] = "favorite_program"
      param[:owner] = @current_user
      param[:user] = Program.find(params[:id]).owner
    end
  end
end