class Api::PlacesController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def index
    client = Foursquare2::Client.new(:client_id => 'Y3Q4CNN34PYN2TO5HEXBNBHX1FIZZE1BFDZ5SOLH2PYQ11MD', :client_secret => '3D0NAXJ2TL5RH5TMKMAFKBZUU3HC41ZFDPRD1C5YE3BONLQI', :api_version => '20131017')
    @entities = client.search_venues(:ll => "#{place_params[:lat]}, #{place_params[:lng]}", :category_id => '4f4528bc4b90abdf24c9de85', :limit => 30,  :query => place_params[:name])
  end

  def show
    @entity = Place.find(params[:id])
  end

  private
  def place_params
    params.require(:place).permit(:lat, :lng, :name)
  end
end