class Api::ProgramCommentsController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def index
    @q = ProgramComment.search(exercise_id_eq: params[:id])
    @entities = @q.result.order(id: :desc).page(params[:page]).per(params[:per_page])
    render 'api/program_comments/index'
  end

  def create
    @entity = ProgramComment.new(comment_params)
    if @entity.save
      UserNotification.create(notification_params) #unless current_resource_owner == Program.find(params[:id]).owner
      render 'api/program_comments/_program_comment'
    else
      respond_with @entity
    end
  end

  def destroy
    @entity = ProgramComment.find(params[:id])
    if @entity.owner?(@current_user)
      @entity.remove_notification
      @entity.destroy
      render 'api/program_comments/_program_comment'
    else
      respond_with @entity
    end
  end

  private
  def comment_params
    params.require(:comment).permit(:text).tap do |param|
      param[:user_id] = @current_user.id
      param[:program_id] = params[:id]
    end
  end

  def notification_params
    params.permit().tap do |param|
      param[:entity_id] = @entity.program.id
      param[:notification_type] = "program_comment"
      param[:owner] = current_resource_owner
      param[:user] = Program.find(params[:id]).owner
    end
  end
end