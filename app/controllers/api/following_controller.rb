class Api::FollowingController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def create
    @following = Follower.new(following_params)

    @user = @following.user
    if @following.save
      UserNotification.create(notification_params)
      return render 'api/following/_follower'
    else
      respond_with @following
    end
  end

  def follow_all
    ids = []
    friends = @current_user.find_fb_friends
    friends.each do |friend|
      ids << friend['id']
    end

    User.where(facebook_id: ids).each do |user|
      unless @current_user.follower?(user.id)
        Follower.create(owner: @current_user, user: user)
        params[:user_id] = user.id
        UserNotification.create!(notification_params)
      end
    end
    return head :ok, status: 200
  end

  def destroy
    @entity = Follower.where(user_id: params[:user_id], owner_id: @current_user.id).first
    @user = @entity.user
    @entity.remove_notification
    if @entity.destroy
      render 'api/following/_follower'
    end
  end

  def followers
    @q = Follower.search(user_id_eq: params[:user_id])
    @entities = @q.result.page(params[:page]).per(params[:per_page])
  end

  def follow
    @q = Follower.search(owner_id_eq: params[:user_id])
    @entities = @q.result.page(params[:page]).per(params[:per_page])
  end

  private
  def following_params
    params.permit().tap do |whitelisted|
      whitelisted[:user] = User.find(params[:user_id])
      whitelisted[:owner] = current_resource_owner
    end
  end

  def notification_params
    params.permit().tap do |param|
      param[:notification_type] = "start_following"
      param[:entity_id] = current_resource_owner.id
      param[:owner] = current_resource_owner
      param[:user] = User.find(params[:user_id]) if params[:user_id].present?
    end
  end
end