class Api::ExerciseTypesController < Api::ApiController
  before_action do
    doorkeeper_authorize!
  end

  def index
    @q = ExerciseType.search(params[:q])
    @entities = @q.result.alphabetically.page(params[:page]).per(params[:per_page])
  end

  def popular
    @q = ExerciseType.search(params[:q])
    @entities = @q.result.order(exercises_counter: :desc).page(params[:page]).per(params[:per_page])
    render 'api/exercise_types/index'
  end

  def show
    @entity = ExerciseType.find(params[:id])
  end

  def metric
    @entity = ExerciseType.find(params[:id]).metric
  end
end