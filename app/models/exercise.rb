class Exercise < ActiveRecord::Base

  #validates :description, :video, :exercise_type_id, :program_id, presence: true

  belongs_to :metrics, :class_name => "ExerciseMetric", :foreign_key => "exercise_metric_id"
  belongs_to :owner, :class_name => "User", :foreign_key => "user_id"
  belongs_to :place

  belongs_to :exercise_type
  belongs_to :program, :class_name => "Program", :foreign_key => "program_id"

  has_many :likes, dependent: :destroy
  has_many :exercise_comments, dependent: :destroy

  has_attached_file :video, :processors => [:ffmpeg], :styles => {
      :medium => { :geometry => "300x300>", :format => 'jpg', :time => 2},
      :thumb => { :geometry => "100x100>", :format => 'jpg', :time => 2 },
      :big => { :geometry => "1280x720>", :format => 'jpg', :time => 2 }
  }

  validates_attachment :video,
                       :content_type => { :content_type => ["video/mp4", "video/avi", "video/mpeg", "video/quicktime", "audio/mpeg", "audio/mp3", "audio/mp4"] },
                       :size => { :in => 0..200.megabytes }

  def increment_exercise_type_counter
    self.exercise_type.increment!(:exercises_counter, 1)
  end


  def decrement_exercise_type_counter
    self.exercise_type.increment!(:exercises_counter, -1)
  end

  def increment_program_counter
    self.program.increment!(:exercises_counter, 1)
  end

  def decrement_program_counter
    self.program.increment!(:exercises_counter, -1)
  end

  def liked?(user)
    likes.where(user: user).exists?
  end

  def owner?(user)
    self.owner == user
  end

  def shift_position
    Exercise.where('program_id = ? and position > ?', self.program_id, self.position).each do |exercise|
      exercise.position -= 1
      exercise.save
    end
  end

  def remove_notification
    UserNotification.where("entity_id = #{self.id} and notification_type = 'create_exercise' or notification_type = 'like_exercise' or notification_type = 'exercise_comment'").delete_all
  end
end