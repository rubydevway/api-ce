class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  validates :user_name, uniqueness: true

  has_many :followers ,dependent: :destroy
  has_many :following, :class_name => "Follower", :foreign_key => "owner_id"

  has_many :programs,  dependent: :destroy
  has_many :devices,  dependent: :destroy
  has_many :favorite_programs,  dependent: :destroy
  has_many :reports, dependent: :destroy
  has_many :exercises,  dependent: :destroy
  has_many :likes,  dependent: :destroy
  has_many :exercise_comments,  dependent: :destroy

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :avatar,:styles => { :medium => "300x300>", :thumb => "100x100>", :big =>"1280x1280>"  }
  validates_attachment :avatar, :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"] },
                       :size => { :in => 0..3.megabytes }

  has_attached_file :cover_photo,:styles => { :medium => "300x300>", :thumb => "100x100>", :big =>"1280x1280>"  }
  validates_attachment :cover_photo, :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"] },
                       :size => { :in => 0..3.megabytes }

  scope :admin, -> { where(role: 'admin') }

  def admin?
    self.role == 'admin'
  end

  def follower?(owner_id)
    if  Follower.where(user_id: owner_id, owner_id: self.id).first
      true
    else
      false
    end
  end

  def reset_push_counter
    self.update_attribute(:push_counter, 0)
  end

  def follow?(user_id)
    Follower.where(user_id: user_id,  owner_id: self.id).exists?
  end

  def top_programs
    self.programs.limit(5)
  end

  def password_required?
    return false unless facebook_id.nil?
    super
  end

  def find_fb_friends

    conn = Faraday.new(:url => 'https://graph.facebook.com') do |faraday|
      faraday.request  :url_encoded
      faraday.adapter  Faraday.default_adapter
    end

    respond = conn.get '/v2.4/me/friends', :access_token => self.facebook_token

    raise ThirdSideError, "Errors receive data  from FB #{ActiveSupport::JSON.decode(respond.body)['error']['message']}" if respond.status != 200

    friends_list = ActiveSupport::JSON.decode(respond.body)

    friends_list["data"]
  end

  def self.get_by_fbtoken access_token

    conn = Faraday.new(:url => 'https://graph.facebook.com') do |faraday|
      faraday.request  :url_encoded
      faraday.adapter  Faraday.default_adapter
    end

    respond = conn.get '/v2.4/me', {  :fields => 'id,email,first_name,last_name,gender,birthday,hometown,picture.width(640),cover',  :access_token => access_token}

    raise ThirdSideError, "Errors receive data  from FB #{ActiveSupport::JSON.decode(respond.body)['error']['message']}" if respond.status != 200

    user_info = ActiveSupport::JSON.decode(respond.body)

    user = User.find_by(:facebook_id => user_info['id'])


    if user.nil?
      user = User.find_by(:email => user_info['email'])
    end

    if user.nil?

      user_params = {
          :email => user_info['email'],
          :user_name => "#{user_info['email'][/[^@]+/]}_#{DateTime.now.to_i}",
          :first_name => user_info['first_name'],
          :last_name => user_info['last_name'],
          :facebook_id => user_info['id'],
          :facebook_token => access_token
      }

      user_params[:avatar] =  URI.parse(user_info['picture']['data']['url']) unless user_info['picture']['data']['url'].nil?
      user_params[:cover_photo] = URI.parse(user_info['cover']['source']) unless user_info['cover'].nil?

      user = User.new(user_params)
      user.save!
    end

    if user.facebook_id.nil?
      user.update_attribute(:facebook_id, user_info['id'])
    end

    return user
  end
end