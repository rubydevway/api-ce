class ExerciseType < ActiveRecord::Base
  belongs_to :metric
  has_many :exercises

  scope :alphabetically, ->{ order("name ASC") }

  has_attached_file :image,:styles => { :medium => "300x300>", :thumb => "100x100>", :big =>"1280x1280>"  }
  validates_attachment :image, :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"] },
                       :size => { :in => 0..3.megabytes}

end