class Like < ActiveRecord::Base

  belongs_to :exercise, :class_name => "Exercise", :foreign_key => "exercise_id"
  belongs_to :user, :class_name => "User", :foreign_key => "user_id"

  validates :exercise, :uniqueness => {:scope => [:user_id, :exercise_id]}

  after_destroy :decrement_exercise_likes_counter
  after_create :increment_exercise_likes_counter

  def decrement_exercise_likes_counter
    self.exercise.increment!(:likes_counter, -1)
  end

  def increment_exercise_likes_counter
    self.exercise.increment!(:likes_counter, 1)
  end

  def remove_notification
    UserNotification.where('entity_id = ? and notification_type = ?', self.exercise_id, "like_exercise" ).first.destroy
  end
end