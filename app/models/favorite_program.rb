class FavoriteProgram < ActiveRecord::Base

  validates :user_id, uniqueness: { scope: :program_id, message: 'This program has already been added to favorites' }

  belongs_to :user, :class_name => "User", :foreign_key => "user_id"
  belongs_to :program, :class_name => "Program", :foreign_key => "program_id"

  before_save :increment_favourites_counter
  after_destroy :decrement_favourites_counter

  def increment_favourites_counter
    self.program.increment!(:favorites_counter, 1)
  end

  def decrement_favourites_counter
    self.program.increment!(:favorites_counter, -1)
  end

  def remove_notification
    UserNotification.where('entity_id = ? and notification_type = ?', self.program_id, "favorite_program" ).first.destroy
  end
end