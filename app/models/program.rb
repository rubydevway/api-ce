class Program < ActiveRecord::Base
  has_many :exercises, dependent: :destroy
  has_many :favorite_programs,  dependent: :destroy
  has_many :program_comments,  dependent: :destroy
  belongs_to :owner, :class_name => "User", :foreign_key => "user_id"

  validates :name, :description, :image, presence: true

  default_scope  { order('id DESC') }

  has_attached_file :image,:styles => { :medium => "300x300>", :thumb => "100x100>", :big =>"1280x1280>"  }
  validates_attachment :image, :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"] },
                       :size => { :in => 0..3.megabytes }
  def favorite?(user_id)
    FavoriteProgram.where(user_id: user_id,  program_id: self.id).exists?
  end

  def increment_programs_counter
    self.owner.increment!(:programs_counter, 1)
  end

  def decrement_programs_counter
    self.owner.increment!(:programs_counter, -1)
  end

  def owner?(user)
    self.owner == user
  end

  def remove_notification
    UserNotification.where("entity_id = #{self.id} and notification_type = 'create_program' or notification_type = 'program_comment' or notification_type = 'favorite_program'").delete_all
  end
end