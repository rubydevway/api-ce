class ProgramComment < ActiveRecord::Base
  belongs_to :user, :class_name => "User", :foreign_key => "user_id"
  belongs_to :program, :class_name => "Program", :foreign_key => "program_id"

  before_save :increment_comments_counter
  after_destroy :decrement_comments_counter

  def owner?(user)
    self.user == user || self.program.owner == user
  end

  def increment_comments_counter
    self.program.increment!(:comments_counter, 1)
  end

  def decrement_comments_counter
    self.program.increment!(:comments_counter, -1)
  end

  def author?(user)
    self.user == user
  end

  def remove_notification
    notification = UserNotification.where('entity_id = ? and notification_type = ?', self.program_id, "program_comment" ).first
    notification.destroy unless notification.nil?
  end
end