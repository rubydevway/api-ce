class ExerciseComment < ActiveRecord::Base
  belongs_to :user, :class_name => "User", :foreign_key => "user_id"
  belongs_to :exercise, :class_name => "Exercise", :foreign_key => "exercise_id"

  before_save :increment_comments_counter
  after_destroy :decrement_comments_counter

  def owner?(user)
    self.user == user || self.exercise.program.owner == user
  end

  def increment_comments_counter
    self.exercise.increment!(:comments_counter, 1)
  end

  def decrement_comments_counter
    self.exercise.increment!(:comments_counter, -1)
  end

  def author?(user)
    self.user == user
  end

  def remove_notification
    UserNotification.where('entity_id = ? and notification_type = ?', self.exercise_id, "exercise_comment" ).first.destroy
  end
end