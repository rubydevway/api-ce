class Follower < ActiveRecord::Base
  belongs_to :owner, :class_name => "User", :foreign_key => "owner_id"
  belongs_to :user, :class_name => "User", :foreign_key => "user_id"

  validates :user, uniqueness: { scope: :owner, message: 'You already following this user' }
  validate :following_yourself

  before_save :increment_followers_counter, :increment_follow_counter
  after_destroy :decrement_followers_counter, :decrement_follow_counter

  def following_yourself
    errors.add(:user, "can't be followed") if user == owner
  end

  def increment_followers_counter
    self.user.increment!(:followers_counter, 1)
  end

  def decrement_followers_counter
    self.user.increment!(:followers_counter, -1)
  end

  def increment_follow_counter
    self.owner.increment!(:following_counter, 1)
  end

  def decrement_follow_counter
    self.owner.increment!(:following_counter, -1)
  end

  def remove_notification
    UserNotification.where('entity_id = ? and notification_type = ?', self.owner_id, "start_following" ).first.destroy
  end
end