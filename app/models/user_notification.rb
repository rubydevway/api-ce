class UserNotification < ActiveRecord::Base
  belongs_to :owner, :class_name => "User", :foreign_key => "owner_id"
  belongs_to :user, :class_name => "User", :foreign_key => "user_id"


  after_create :push_notification

  def push_notification

    messages = {
        "start_following" => "started following you",
        "program_comment" => "commented your program",
        "exercise_comment" => "commented your exercise",
        "like_exercise" => "liked your exercise",
        "favorite_program" => "favorited your program"
    }

    NotificationsWorker.perform_async(self.notification_type, self.user_id, self.entity_id, messages["#{self.notification_type}"], self.owner_id) unless self.owner == self.user
  end
end