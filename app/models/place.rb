class Place < ActiveRecord::Base
  has_many :exercises

  def self.find_by_foursquare_id fsq_id
    place = Place.find_by(:foursquare_id => fsq_id)
    unless place.blank?
      place.increment_exercise_type_counter
      place.save!
      place
    else
      nil
    end
  end

  def increment_exercise_type_counter
    self.increment!(:exercises_counter, 1)
  end

  def decrement_exercise_counter
    self.increment!(:exercises_counter, -1)
  end
end
